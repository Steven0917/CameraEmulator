

Camera Emulator Revision History.



Version 1.2

1) Remove unnecessary files / interface
2) update test result.


------------------------------------------------------

Version 1.1

1) Provide ability to change the integration time. An adaptive integration time targets to an expected capture FPS.
2) Maintain a typecal capture frame rate of 30 Hz when integration time is lower than 30 ms.
3) Generate video process result and represent on console: ImageID, TimeStamp Prod(Producer), TimeStamp Recd(Consumer),DiffTimeStamps, ImageMean, FPS
4) Fixed a defect in FPS calc.


------------------------------------------------------

Version 1.0

1) Implemented the image capture with the ability to generate frames which keeps Image ID, Time Stamp Prod/Recd and also the actural payload.
2) Maintain an image stream with a maxNumberofImages = 50. It sync the working threads between ImageCapturer and ImageProcessor.
3) Enable the capability to process received frames by providing ImageMean and also the latency bewteen producer and consumer(as DiffTimeStamps).
4) Enable caputure FPS calculation based on latest 5 frames received.


------------------------------------------------------

Version 0.1

Initial design of an event driven program. A simple implmentation of producer-Consumer model.
Testing done to ensure the concurrent processing capability. 

  ImageCapturer:   Image frame producer
  ImageProcessor:  Image frame consumer 
  ImageStream:     Image buffer queue keeping image frames produced by ImageCapturer and consumed by ImageProcessor
  ImageFrame:      'GrabResultPtr' equivalent instance which keeps the image buffer and necessary infomations about the frame. It's passing between ImageCapturer and ImageProcessor
  CameraEmulator:  Camera appliation 
