#pragma once
#include <vector>
#include <chrono>

using namespace std;

#define DEFAULT_RESOLUTION_WIDTH  640
#define DEFAULT_RESOLUTION_HEIGHT  480

class ImageFrame
{
public:
	ImageFrame(int width = DEFAULT_RESOLUTION_WIDTH, int height = DEFAULT_RESOLUTION_HEIGHT);
	~ImageFrame();

	void GenerateFrame();
	float CalcImageMean();
	float GetImageMean();

	void SetProducedTime(chrono::system_clock::time_point time = chrono::system_clock::now());
	chrono::system_clock::time_point GetProducedTime();

	void SetProcessedTime(chrono::system_clock::time_point time = chrono::system_clock::now());
	chrono::system_clock::time_point GetProcessedTime();
	
	unsigned long GetImageID();

protected:
	// Resolution
	int mWidth;
	int mHeight;

	//ImageID
	unsigned long mImageID;

	//TimeStamp Produced
	chrono::system_clock::time_point mProducedTime;

	//TimeStamp Processed
	chrono::system_clock::time_point mProcessedTime;

	//Integration Time
	chrono::system_clock::time_point mIntegrationTime;

	//Image buffer 
	vector<uint16_t> mData;

	//Image Mean
	float mImageMean;

	static unsigned long id;
};

