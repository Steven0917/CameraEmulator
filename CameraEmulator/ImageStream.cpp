#include "stdafx.h"
#include "ImageStream.h"


ImageStream::ImageStream(int bufSize) : mBufSize(bufSize)
{
}

ImageStream::~ImageStream()
{
}

bool ImageStream::IsEmpty()
{
	lock_guard<mutex> guard(mMutex);
	return mStreamBuf.empty();
}

bool ImageStream::IsFull()
{
	lock_guard<mutex> guard(mMutex);
	return mStreamBuf.size() >= mBufSize;
}

void ImageStream::PushFrame(shared_ptr<ImageFrame>& frame)
{
	lock_guard<mutex> guard(mMutex);
	mStreamBuf.push(frame);
}

shared_ptr<ImageFrame> ImageStream::GetFrame()
{
	lock_guard<mutex> guard(mMutex);

	if (!mStreamBuf.empty())
	{
		shared_ptr<ImageFrame> msg;
		msg = mStreamBuf.front();
		mStreamBuf.pop();
		return msg;
	}

	return shared_ptr<ImageFrame>(nullptr);
}
