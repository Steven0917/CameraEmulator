#include "stdafx.h"
#include "ImageProcessor.h"
#include <iostream>
#include <ctime>

#define DEFAULT_FRAMES_FOR_FPS 5 

ImageProcessor::ImageProcessor(ImageStream& stream)
	: mStream(stream), mStop(true)
{
}


ImageProcessor::~ImageProcessor()
{
}

void ImageProcessor::Start()
{
	mStop = false;
	while (!mStop)
	{
		// Process image frames.
		ProcessFrame();
	}
}

void ImageProcessor::Stop()
{
	mStop = true;
}

void ImageProcessor::ProcessFrame()
{
	if (!mStream.IsEmpty())
	{
		auto frame = mStream.GetFrame();
		frame->CalcImageMean();
		frame->SetProcessedTime();

		cout << endl << "Image ID: " << frame->GetImageID() << " has been processed." << endl;
		ShowTimeStamp(frame->GetProducedTime(), string("TimeStamp Prod: "));
		ShowTimeStamp(frame->GetProcessedTime(), string("TimeStamp Recd: "));
		ShowDiffTimeStamp(frame);
		cout << "Image Mean: " << frame->GetImageMean() << endl;
		RecordProducedTimeStampe(frame->GetProcessedTime());
		ShowFrameRate();
	}
}

void ImageProcessor::ShowTimeStamp(system_clock::time_point& time, string& head)
{
	time_t now_c = chrono::system_clock::to_time_t(time);
	tm now_tm;
	localtime_s(&now_tm, &now_c);
	char buffer[32];
	strftime(buffer, 32, "%Y-%m-%d %H:%M:%S", &now_tm);
	cout << head.c_str() << buffer << endl;
}

void ImageProcessor::ShowDiffTimeStamp(shared_ptr<ImageFrame>& frame)
{
	chrono::duration<float> diff = frame->GetProcessedTime() - frame->GetProducedTime();
	cout << "Diff TimeStamp (Recd - Prod): " << diff.count() * 1000 << " Microseconds." << endl;
}

void ImageProcessor::ShowFrameRate()
{
	float fps = 0.0;
	if (mTimeStampsProduced.size() > 1)
	{
		chrono::duration<float> diff = mTimeStampsProduced.back() - mTimeStampsProduced.front();
		float sec = diff.count();
		fps = float(mTimeStampsProduced.size() - 1) / sec;
	}
	cout << "Caputure FPS (last " << mTimeStampsProduced.size() << " frames): " << fps << endl;
}

void ImageProcessor::RecordProducedTimeStampe(system_clock::time_point& time)
{
	if (mTimeStampsProduced.size() >= DEFAULT_FRAMES_FOR_FPS)
	{
		mTimeStampsProduced.pop();
	}

	mTimeStampsProduced.push(time);
}
