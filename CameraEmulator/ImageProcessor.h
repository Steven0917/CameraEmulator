#pragma once
#include "ImageStream.h"
#include <chrono>
#include <queue>

using std::queue;
using std::chrono::system_clock;

class ImageProcessor
{
public:
	ImageProcessor(ImageStream& stream);
	~ImageProcessor();

	void ProcessFrame();
	void Start();
	void Stop();
	void ShowTimeStamp(system_clock::time_point& time, string& head);
	void ShowDiffTimeStamp(shared_ptr<ImageFrame>& frame);
	void ShowFrameRate();
	void RecordProducedTimeStampe(system_clock::time_point& time);

protected:
	ImageStream& mStream;
	bool mStop;

	// lastest frames for FPS calculation
	queue<system_clock::time_point> mTimeStampsProduced;

};

