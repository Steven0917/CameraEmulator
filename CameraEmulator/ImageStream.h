#pragma once

#include <queue>
#include <mutex>
#include <memory>
#include "ImageFrame.h"

#define DEFAULT_BUFFER_SIZE  50

using namespace std;

class ImageStream
{
public:
	ImageStream(int bufSize = DEFAULT_BUFFER_SIZE);
	~ImageStream();
	bool IsEmpty();
	bool IsFull();
	void PushFrame(shared_ptr<ImageFrame>& frame);
	shared_ptr<ImageFrame> GetFrame();

protected:
	mutex mMutex;
	queue<shared_ptr<ImageFrame>> mStreamBuf;
	int mBufSize;
};



