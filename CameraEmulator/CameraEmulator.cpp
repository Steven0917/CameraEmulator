#include "stdafx.h"
#include "CameraEmulator.h"
#include <iostream>

using namespace std;

void StartImageCapture(ImageCapturer& capture)
{
	capture.Start();
}

void StartImageProcessor(ImageProcessor& processor)
{
	processor.Start();
}

CameraEmulator::CameraEmulator()
{
}

CameraEmulator::~CameraEmulator()
{
}

void CameraEmulator::Run()
{
	ImageStream stream;
	ImageCapturer capturer(stream);
	ImageProcessor processor(stream);
	
	// Set Integration Time to 33.0 ms, target FPS shall be 30
	capturer.SetIntegrationTime(33.0);
	

	// Producer thread: Image Capturer
	thread thread1(StartImageCapture, ref(capturer));

	// Consumer thread: Image Processor
	thread thread2(StartImageProcessor, ref(processor));

	this_thread::sleep_for(std::chrono::seconds(1));

	// Set Integration Time to 50 ms, target FPS shall be around 20
	capturer.SetIntegrationTime(50.0);
	this_thread::sleep_for(std::chrono::seconds(1));

	// Set Integration Time to 20 ms, effective 30 ms with target FPS 33
	capturer.SetIntegrationTime(20.0);
	this_thread::sleep_for(std::chrono::seconds(1));

	// Set Integration Time to 60 ms, target FPS shall be around 16
	capturer.SetIntegrationTime(60.0);
	this_thread::sleep_for(std::chrono::seconds(1));

	// Set Integration Time to 10 ms, effective 30 ms with target FPS 33
	capturer.SetIntegrationTime(10.0);
	this_thread::sleep_for(std::chrono::seconds(1));

	capturer.Stop();
	processor.Stop();

	// Join threads.
	thread1.join();
	thread2.join();
}
