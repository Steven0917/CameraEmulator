#pragma once
#include "ImageCapturer.h"
#include "ImageProcessor.h"

class CameraEmulator
{
public:
	CameraEmulator();
	~CameraEmulator();

	void Run();
};

