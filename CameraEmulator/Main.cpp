﻿// CameraEmulator console application.
//

#include "stdafx.h"
#include "CameraEmulator.h"


int main()
{
	CameraEmulator emulator;
	emulator.Run();

    return 0;
}

