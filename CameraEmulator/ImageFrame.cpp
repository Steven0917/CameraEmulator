#include "stdafx.h"
#include "ImageFrame.h"


unsigned long ImageFrame::id = 0;

ImageFrame::ImageFrame(int width, int height)
	: mWidth(width), mHeight(height), mImageID(id++), mImageMean(0)
{
	GenerateFrame();
	SetProducedTime();
}

ImageFrame::~ImageFrame()
{
}

// For now we put random date for test purpose.
void ImageFrame::GenerateFrame()
{
	srand(time(nullptr) + mImageID);
	mData.resize(mWidth * mHeight);
	for (int i = 0; i < mWidth * mHeight; i++)
	{
		mData[i] = uint16_t(rand() % 65535);
	}
}

float ImageFrame::CalcImageMean()
{
	float sum = 0.0;
	
	int x = 0;
	int y = 0;

	for (int i = 0; i < mWidth; i++)
	{
		float temp = 0.0;
		for (int j = 0; j < mHeight; j++)
		{
			temp += mData[i * mHeight + j];
		}
		temp /= mHeight;

		sum += temp;
	}

	mImageMean = sum / mWidth;
	return mImageMean;
}

float ImageFrame::GetImageMean()
{
	return mImageMean;
}

void ImageFrame::SetProducedTime(chrono::system_clock::time_point time)
{
	mProducedTime = time;
}

chrono::system_clock::time_point ImageFrame::GetProducedTime()
{
	return mProducedTime;
}

void ImageFrame::SetProcessedTime(chrono::system_clock::time_point time)
{
	mProcessedTime = time;
}

chrono::system_clock::time_point ImageFrame::GetProcessedTime()
{
	return mProcessedTime;
}

unsigned long ImageFrame::GetImageID()
{
	return mImageID;
}
