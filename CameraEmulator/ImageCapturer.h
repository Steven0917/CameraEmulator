#pragma once
#include "ImageStream.h"
#include <chrono>

using std::chrono::system_clock;

class ImageCapturer
{
public:
	ImageCapturer(ImageStream& stream);
	~ImageCapturer();

	// Capture a frame
	void Capture();

	// Set Integration time
	void SetIntegrationTime(float ms);

	// Start working process
	void Start();

	// Stop capturing frames
	void Stop();

protected:
	// Integration time, 30 ms by default, this impacts FPS
	float  mIntegrationTime;

	// Image buffer
	ImageStream& mStream;
	
	// flag to stop
	bool mStop;

	// Previous frame timestamp
	static system_clock::time_point sPrevFrameTimeStamp;

};

