#include "stdafx.h"
#include "ImageCapturer.h"
#include <iostream>

#define DEFAULT_INTEGRATION_TIME_MS  30.0

using namespace std;

system_clock::time_point ImageCapturer::sPrevFrameTimeStamp = chrono::system_clock::now();

ImageCapturer::ImageCapturer(ImageStream& stream)
	: mStream(stream), mStop(true), mIntegrationTime(float(DEFAULT_INTEGRATION_TIME_MS))
{
}


ImageCapturer::~ImageCapturer()
{
}

void ImageCapturer::Start()
{
	mStop = false;
	while (!mStop)
	{
		// Caputure an image frame.
		Capture();
	}
}

void ImageCapturer::Stop()
{
	mStop = true;
}

void ImageCapturer::Capture()
{
	if (!mStream.IsFull())
	{
		auto frame = make_shared<ImageFrame>();
		chrono::system_clock::time_point ts = chrono::system_clock::now();
		chrono::duration<float> diff = ts - sPrevFrameTimeStamp;
		float ms = diff.count() * 1000;

		while (ms < mIntegrationTime)
		{
			ts = chrono::system_clock::now();
			diff = ts - sPrevFrameTimeStamp;
			ms = diff.count() * 1000;
		}
				
		frame->SetProducedTime(ts);
		sPrevFrameTimeStamp = frame->GetProducedTime();
		mStream.PushFrame(frame);
	}
}

void ImageCapturer::SetIntegrationTime(float ms)
{
	mIntegrationTime = max(ms, float(DEFAULT_INTEGRATION_TIME_MS));
	cout << endl << endl << endl << "============ Set Integration Time = " << ms << " ms, effective: "
		 << mIntegrationTime << " ms, target FPS " << 1000 / mIntegrationTime << " ================="
		 << endl;
}